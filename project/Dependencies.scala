import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val scalaXml = "org.scala-lang.modules" %% "scala-xml" % "1.1.0"
  lazy val fastparse = "com.lihaoyi" %% "fastparse" % "1.0.0"
}
