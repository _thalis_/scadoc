package scadoc.parser

import fastparse.all._
import scadoc.parser.ScadocParser._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object FastParseScadocParser extends ScadocParser {

  // Define the parser combinators for parsing Asciidoc(tor) documents
  // according to https://asciidoctor.org/docs/user-manual/#basic-document-anatomy
  // based on the logic at http://www.lihaoyi.com/fastparse

  private[parser] lazy val asciiDoc: Parser[Document] = P(Start ~ header.? ~ preamble.? ~ section.rep ~ End)
    .map(parsedHeader => Document(parsedHeader, None, List()))

  private[parser] lazy val header: Parser[Header] =
    P(attribute.rep ~ docTitle.? ~ authorInfos.? ~ revisionInfos.? ~ attribute.rep) map {
      case (parsedPreAttributes, parsedDocTitle, parsedAuthorInfos, parsedRevisionInfos, parsedPostAttributes) =>
        Header(parsedDocTitle, parsedAuthorInfos, parsedRevisionInfos, parsedPreAttributes ++ parsedPostAttributes)
    }

  private[parser] lazy val docTitle: Parser[DocTitle] =
    P("=" ~ space.rep(min = 1) ~/ (!"//" ~ AnyChar).rep(1).! ~ lineEnding).map {
      title => {
        val titleParts = title.split(':')
        if (titleParts.length > 1)
          DocTitle(titleParts.slice(0, titleParts.length - 1).mkString(":").trim, Some(titleParts.last.trim))
        else
          DocTitle(title.trim, None)
      }
    }

  private[parser] lazy val authorInfos: Parser[Seq[AuthorInfo]] = authorInfo.rep

  private[parser] lazy val authorInfo: Parser[AuthorInfo] =
    P(((!space.rep(min = 1)).! ~ space.rep).rep(min = 1) ~ email.?.! ~ (";" | lineEnding)) map {
      case (names, mail) => AuthorInfo(PersonName(names), mail)
    }

  private[parser] lazy val email: Parser[EmailAddress] =
    P("<" ~ (!"@".rep(min = 1)).! ~ "@" ~ (!">".rep(min = 1)).! ~ ">") map {
      case (parsedLocalPart, parsedDomain) => EmailAddress(parsedLocalPart, parsedDomain)
    }

  private[parser] lazy val attribute: Parser[Attribute] = P(":" ~ CharPred(_.isLetter).rep.! ~ ":" ~ newLine)
    .map(attributeName => Attribute(attributeName, None))

  private[parser] lazy val space = P(" ")

  private[parser] lazy val comment = multiLineComment | singleLineComment

  private[parser] lazy val multiLineComment = P("///" ~/ (!"///" ~ AnyChar).rep ~ "///" ~ space.rep ~ (newLine | End))

  private[parser] lazy val singleLineComment = P("//" ~/ AnyChar.rep ~ space.rep ~ (newLine | End))

  private[parser] lazy val lineEnding = P(space.rep ~ (comment | newLine | End))

  private[parser] lazy val newLine = P("\r".? ~ "\n" | "\r")

  // dummy
  private[parser] lazy val preamble = Pass
  private[parser] lazy val section = Pass
  private[parser] lazy val revisionInfos: Parser[VersionInfo] = ???

  override def parse(asciiDoc: String): Future[Document] = Future {
    FastParseScadocParser.asciiDoc.parse(asciiDoc).get.value
  }

}
