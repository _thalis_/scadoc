package scadoc.parser

import java.time.Instant

import scadoc.parser.ScadocParser.Document

import scala.concurrent.Future

trait ScadocParser {
  def parse(asciiDoc: String): Future[Document]
}

object ScadocParser {

  case class Document(header: Option[Header], preamble: Option[Preamble], sections: Seq[Section])

  case class Header(docTitle: Option[DocTitle], autorInfos: Option[Seq[AuthorInfo]], versionInfo: Option[VersionInfo],
                    attributes: Seq[Attribute])

  case class AuthorInfo(name: PersonName, email: Option[EmailAddress])

  case class DocTitle(mainTitle: String, subTitle: Option[String])

  case class PersonName(prename: Option[String], middlename: Option[String], surname: String)

  object PersonName {
    def apply(names: Seq[String]) = {
      names.last
    }
  }

  case class EmailAddress(localPart: String, domain: String)

  case class VersionInfo(version: String, date: Option[Instant], revRemark: Option[String])

  case class Attribute(name: String, value: Option[String])

  case class Preamble(paragraphs: Seq[Paragraph])

  sealed trait Section {
    def title: String

    def paragraphs: Seq[Paragraph]
  }

  case class Section1(title: String, paragraphs: Seq[Paragraph]) extends Section

  case class Section2(title: String, paragraphs: Seq[Paragraph]) extends Section

  case class Section3(title: String, paragraphs: Seq[Paragraph]) extends Section

  case class Paragraph(text: String)

}
