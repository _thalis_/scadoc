package scadoc.parser

import fastparse.core.Parsed
import fastparse.core.Parsed.{Failure, Success}
import org.scalatest.{FlatSpec, Matchers, OptionValues}

class HeaderParserSpec extends FlatSpec with Matchers with OptionValues {

  "The FastParse header parser" should "parse the commented and attributed header as valid" in {

    val commentedAndAttributedHeader =
      """// Header
        |// single line comment
        |/// THis is
        |a multiline
        |comment///
        |= A Cautionary Tale: The Dangerous and Thrilling Documentation Chronicles: A Tale of Caffeine and Words
        |Kismet Rainbow Chameleon <kismet@asciidoctor.org>; Lazarus het_Draeke <lazarus@asciidoctor.org>
        |v1.0, October 2, 2013: First incarnation
        |:attr1: wert
        |///
        |:no_attr:
        |Another multiline comment
        |///
        |// :no_attr2 :
        |:flag_attr:"""

    val Parsed.Success(header, _) = FastParseScadocParser.header.parse(commentedAndAttributedHeader)

    "The parsed have" should "have title 'A Cautionary Tale: The Dangerous and Thrilling Documentation Chronicles'" in {

      val docTitle = header.docTitle.getOrElse(fail)
      assert(docTitle.mainTitle === "A Cautionary Tale: The Dangerous and Thrilling Documentation Chronicles")

      "and the subtitle of that document" should "be 'A Tale of Caffeine and Words'" in {
        assert(docTitle.subTitle.getOrElse(fail) === "A Tale of Caffeine and Words")
      }
    }
  }

  "A doc title without a colon" should "have just a main title but no subtitle" in {

    val title = "= The main title  "
    val docTitle = FastParseScadocParser.docTitle.parse(title).get.value

    assert(docTitle.mainTitle === "The main title")
    assert(docTitle.subTitle === None)
  }

  "A doc title with more than one colon" should "have the last part as subtitle" in {

    val title = "= A Cautionary Tale: The Dangerous and Thrilling Documentation Chronicles: A Tale of Caffeine and Words"
    val docTitle = FastParseScadocParser.docTitle.parse(title).get.value

    assert(docTitle.mainTitle === "A Cautionary Tale: The Dangerous and Thrilling Documentation Chronicles")
    assert(docTitle.subTitle.getOrElse(fail) === "A Tale of Caffeine and Words")
  }

  "A doc title ending with a single line comment" should "be considered valid" in {

    val title = "= Commented main title // and here is it"
    val docTitle = FastParseScadocParser.docTitle.parse(title).get.value

    assert(docTitle.mainTitle === "Commented main title")
    assert(docTitle.subTitle === None)
  }

  "A doc title with subtitle ending with a multiline comment" should "be considered valid" in {

    val title =
      """= That's the main title: and I am the subtitle /// and that
        | is the multiline
        | comment for the author of the document. ///
        |
      """
    val docTitle = FastParseScadocParser.docTitle.parse(title).get.value

    assert(docTitle.mainTitle === "That's the main title")
    assert(docTitle.subTitle.getOrElse(fail) === "and I am the subtitle")
  }

  "A doc title having no space between the title sign and the title" should "be considered invalid" in {
    FastParseScadocParser.docTitle.parse("=Invalid document title") match {
      case Success(_,_) => fail("That invalid doc title should not be successfully parsed")
      case Failure(_,_,_) => succeed
    }

  }

  "An author with multiple authors" should "have more than one author" in {
    FastParseScadocParser.authorInfos.parse("Kismet Rainbow Chameleon <kismet@asciidoctor.org>;" +
      " Lazarus het_Draeke <lazarus@asciidoctor.org>") match {
      case Failure(_,_,_) => fail("The author line having multiple author infos should be considered valid")
      case Success(authorInfos,_) =>
        assert(authorInfos.size === 2)
        val kismet = authorInfos.head
        assert(kismet.name.surname === "Chameleon")
        assert(kismet.name.prename.value === "Kismet")
        assert(kismet.name.middlename.value === "Rainbow")
        assert(kismet.email.value.localPart === "kismet")
        assert(kismet.email.value.domain === "asciidoctor.org")
        assert(kismet.email.value === "kismet@asciidoctor.org")
        val lazarus = authorInfos(1)
        assert(lazarus.name.surname === "het Draeke")
        assert(lazarus.name.prename.value === "Lazarus")
        assert(lazarus.name.middlename.isEmpty)
        assert(lazarus.email.value.localPart === "kismet")
        assert(lazarus.email.value.domain === "asciidoctor.org")
        assert(lazarus.email.value === "kismet@asciidoctor.org")
    }
  }

  "A single line comment" should "be recognized as such" in {

    val singleLineComment = "// a comment"
    val commentResult = FastParseScadocParser.comment.parse(singleLineComment).get match {
      case Success(_,_) => succeed
      case _ => fail
    }

  }

  "A multiline comment" should "be recognized as such" in {

    val multilineComment =
      """/// That's
        |a multiline
        | comment
        |  with ignored
        |intendation
        |///"""

    val commentResult = FastParseScadocParser.multiLineComment.parse(multilineComment).get match {
      case Success(_,_) => succeed
      case _ => fail
    }
  }
}
